! update_av_scalars_module - update averaged scalar parameters
! Copyright (C) 2016, 2017 B. Roukema  under the Cecill licence
! See http://www.cecill.info for licence details; the licence is
! GPL-compatible.

! Within the main MPI process (root cpu):

! Given the scalar averaged parameters estimated for a particular
! time step, on a regular grid, via DTFE, store these in a rolling
! record of at most n_steps_max steps, within this main MPI process.
! When the number of stored records is sufficient, shift the
! n_steps_max steps each to one time step earlier, except for
! the earliest one, which is overwritten and lost.
! Integrate the Raychaudhuri equation, including the role of
! kinematical backreaction, to obtain the n_steps_max-th
! estimate of the effective scale factor.

! The outputs are the global effective scale factor and the expansion
! rate, and the fact that they have been calculated, written to
! a_D_common and H_D_common, declared in amr_parameters.


module update_av_scalars_global_module

#define TOL_RAY_FIRST_ORDER_MIN 1d-20
#define TOL_RAY_FIRST_ORDER_MAX 1d6

  use amr_commons, only : t ! Warning: "t" is a one-letter, effectively global variable

  use amr_parameters, only : dp, i4b, &
       h0, aexp, hexp, texp, &
       omega_m, &
       a_D_common, H_D_common, &
       have_a_D_estimate, have_H_D_estimate, &
       scal_av_n_gridsize, &
       scal_av_biscale, &
       scal_av_n_gridsize1, &
       scal_av_n_gridsize2, &
       scal_av_n_gridsize3, &
       scal_av_dt_fact, &
       scal_av_use_a_FLRW, &
       scal_av_use_H_FLRW, &
       scal_av_set_Q_to_zero, &
       scal_av_Q_include_vort, &
       scal_av_use_omega_m_FLRW, &
       scal_av_no_feedback

  !use curved_volume_module !TODO: the module is written, but not yet used

#ifdef CHECK_V_SCALE
  real(dp),allocatable,dimension(:,:)::xp_global_old  ! old positions
#endif

  ! This is for convenience for amr_dtfe_interface
  real(dp) :: hexp_invGyr ! FLRW expansion rate converted to inverse Julian Gyr


contains

  subroutine update_av_scalars_global(&
       ! INPUTS
       divergence_av, &
       divergence_sq_av, &
       IIinv_av, &
       shear_av, &
       vorticity_av, &
       dtfe_density_av, &
       Eul_vol_tot, &
       f_antiflow, &
       last_step &
       )

#ifndef WITHOUTMPI
#ifndef OLD_MPI_F77_STYLE
    use mpi_f08
#endif
#endif
    implicit none
#ifndef WITHOUTMPI
#ifdef OLD_MPI_F77_STYLE
    include 'mpif.h'
#endif
#endif

    ! INPUTS
    real(dp), intent(in), allocatable,dimension(:,:,:) :: divergence_av ! of vel gradient
    real(dp), intent(in), allocatable,dimension(:,:,:) :: divergence_sq_av ! div squared
    real(dp), intent(in), allocatable,dimension(:,:,:) :: IIinv_av ! second invariant of vel gradient
    ! shear map
    real(dp), intent(in), allocatable, dimension(:,:,:) :: shear_av
    ! vorticity map
    real(dp), intent(in), allocatable, dimension(:,:,:) :: vorticity_av
    ! density map
    real(dp), intent(in), allocatable, dimension(:,:,:) :: dtfe_density_av
    ! Eulerian total volume of each subdomain; for analysis only
    real(dp), intent(in), allocatable, dimension(:,:,:) :: Eul_vol_tot

    real(dp) :: f_antiflow ! rough estimator half of virialisation fraction

    logical, intent(in) :: last_step

    ! INTERNAL
    integer(i4b) :: i_mpi_rank
    integer(i4b) :: ierr_scalav

    ! Integrability constraint (e.g. arXiv:1303.6193 Eq. (12))
    integer, save :: n_steps_available = 0 ! available for integration
    integer, parameter :: n_steps_max = 3 ! max steps to store
    real(dp), parameter :: t_NULL = -9e9
    ! store history of background model (FLRW) values
    real(dp), save :: t_FLRW(n_steps_max)
    real(dp), save :: a_FLRW(n_steps_max)
    real(dp), save :: H_FLRW(n_steps_max)

    logical, save :: first_step = .true.
    integer, save :: early_step_count = 0
    integer, parameter :: early_step_count_max = (n_steps_max)
    integer :: i_steps ! stored-time-step iterator

    ! TODO: Curvature (3-Ricci scalar curvature) is not yet
    ! calculated directly.
    real(dp),save, allocatable,dimension(:,:,:,:) :: divergence_history ! divergence history
    real(dp),save, allocatable,dimension(:,:,:,:) :: IIinv_history ! IInv invariant history
    real(dp),save, allocatable,dimension(:,:,:,:) :: Q_history ! Q history
    real(dp),save, allocatable,dimension(:,:,:,:) :: density_history ! density
    ! *_history: Scalar-averaged scale factor and expansion rate
    ! estimates in a subdomain.
    real(dp),save, allocatable,dimension(:,:,:,:) :: a_D_local_history
    real(dp),save, allocatable,dimension(:,:,:,:) :: dot_a_D_local_history
    ! TODO: Add the equation in Roukema et al 2017 for Aconst
    real(dp),save, allocatable,dimension(:,:,:) :: Aconst
    real(dp) :: Q_mean, Q_stddev ! statistics accumulators
    real(dp) :: delta_Q ! Q_new - Q_old
    real(dp) :: delta_Q_mean, delta_Q_stddev ! statistics accumulators
    ! very generous tolerances for avoiding zero
#define TOL_DENSITY (1d-20)
#define TOL_A_D_VIRIAL_RATIO 0.16d0
#define DOT_A_D_DELTA_T_VIRIAL_MIN (-100.0d0)


    real(dp), save :: a_D(n_steps_max) ! global scale factor
    real(dp), save :: H_D(n_steps_max) ! global expansion factor

    real(dp), save :: n_subdomains

    real(dp) :: hexp_kmsMpc ! FLRW expansion rate converted to km/s/Mpc
    !real(dp) :: hexp_invGyr ! FLRW expansion rate converted to inverse Julian Gyr

    ! Unit conversion factors - see amr/units.f90
    ! s to km conversion constant
    !real(dp), parameter :: c_spacetime_kms = 299792.458
    !real(dp) :: c_H ! c_spacetime/(expansion rate)
    real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
    ! Convert default ramses cosmo time units into conventional FLRW
    ! time units:
    real(dp)::scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr
    real(dp)::scale_hexp_invGyr, scale_hexp_kmsMpc

    ! 1 Mpc/Gyr in terms of km/s
    real(dp), parameter :: kms_per_MpcGyr = 977.79_dp


    ! DTFE grid iterators
    integer :: ip,jp,kp

    real(dp) :: t_zero_Gyr_EdS ! EdS age of Universe in Gyr (Julian)
    real(dp) :: delta_t_FLRW ! FLRW time interval

    !! unit conversion !!
    call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

    call units_kpc_Gyr(scale_l_kpc,scale_t_Gyr,scale_v_kpcGyr, &
         scale_hexp_invGyr, scale_hexp_kmsMpc)

    t_zero_Gyr_EdS = 2.0d0/(3.0d0* h0) *kms_per_MpcGyr


    hexp_invGyr = hexp *scale_hexp_invGyr
    hexp_kmsMpc = hexp *scale_hexp_kmsMpc

    if(first_step)then
       allocate(Q_history( &
            n_steps_max, &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))
       allocate(divergence_history( &
            n_steps_max, &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))
       allocate(IIinv_history( &
            n_steps_max, &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))
       allocate(density_history( &
            n_steps_max, &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))
       allocate(a_D_local_history( &
            n_steps_max, &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))
       allocate(dot_a_D_local_history( &
            n_steps_max, &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))
       allocate(Aconst( &
            scal_av_n_gridsize1, scal_av_n_gridsize2, &
            scal_av_n_gridsize3))

       n_subdomains = real(scal_av_n_gridsize1,dp)* &
            real(scal_av_n_gridsize2,dp)* &
            real(scal_av_n_gridsize3,dp)

       write(*,'(a,g15.1)')'Using scal av Raychaudhuri integrator... n_subdomains = ', &
            & n_subdomains
#ifndef WITHOUTMPI
       call MPI_COMM_RANK(MPI_COMM_WORLD, i_mpi_rank, ierr_scalav)
       if(0 == i_mpi_rank)then
          write(*,'(a,i8,a)')'cpu = ',i_mpi_rank,' = root cpu.'
       else
          write(*,'(a,i8,a)')'ERROR: cpu = ',i_mpi_rank,' which is not the root cpu.'
          stop ! or call clean_stop if it works correctly
       endif
#endif /* WITHOUTMPI */

       first_step = .false. ! for the root cpu only, and only once
    endif

    if(early_step_count < early_step_count_max)then
       early_step_count = early_step_count+1
    else
       print*,'early_step_count = ',early_step_count,'reached.'
    endif


    ! If available, shift stored data at each step to one step
    ! earlier; and store present data. Several parameters of the
    ! "present data" at n_steps_available are overwritten
    ! in calculations in the block after shift_and_store.
    shift_and_store: if(n_steps_available+1 .le. n_steps_max)then
       ! Store present data, since max set of steps not yet full.
       t_FLRW(n_steps_available+1) = texp *scale_t_Gyr ! global time in Julian Gyr
       a_FLRW(n_steps_available+1) = aexp ! FLRW scale factor
       H_FLRW(n_steps_available+1) = hexp_invGyr ! FLRW expansion rate

       ! a_D should be overwritten for non-first steps
       a_D(n_steps_available+1) = aexp ! global rigid-expansion scale factor

       ! Store Q. Also store some of the parameters that contribute
       ! to Q, although this is not really needed.
       do ip = 1,scal_av_n_gridsize1
          do jp = 1,scal_av_n_gridsize2
             do kp = 1,scal_av_n_gridsize3
                divergence_history(n_steps_available+1, ip,jp,kp) = &
                     divergence_av(ip,jp,kp) ! Iinv \equiv divergence

                IIinv_history(n_steps_available+1, ip,jp,kp) = &
                     IIinv_av(ip,jp,kp)

                if(scal_av_set_Q_to_zero)then
                   Q_history(n_steps_available+1, ip,jp,kp) = 0_dp
                elseif(scal_av_Q_include_vort)then
                   Q_history(n_steps_available+1, ip,jp,kp) = &
                        2.0_dp * IIinv_av(ip,jp,kp) &
                        - (2.0_dp/3.0_dp) * &
                        divergence_av(ip,jp,kp)**2
                else
                   Q_history(n_steps_available+1, ip,jp,kp) = &
                        2.0_dp * ( & !vorticity_av(ip,jp,kp)  &
                        - shear_av(ip,jp,kp) ) + &
                        (2.0_dp/3.0_dp) * &
                        ( divergence_sq_av(ip,jp,kp) - &
                        divergence_av(ip,jp,kp)**2 )
                endif

                if(scal_av_use_omega_m_FLRW)then
                   density_history(n_steps_available+1, ip,jp,kp) = &
                        omega_m ! this means \Omega_{m0}^{FLRW}
                else
                   density_history(n_steps_available+1, ip,jp,kp) = &
                        dtfe_density_av(ip,jp,kp)
                endif

                a_D_local_history(n_steps_available+1, ip,jp,kp) = &
                     aexp ! rigid-expansion (FLRW) scale factor for early time steps

                dot_a_D_local_history(n_steps_available+1, ip,jp,kp) = &
                     hexp_invGyr * aexp & ! rigid-expansion (FLRW) expansion rate for early time steps
                     * (1.0 + (divergence_av(ip,jp,kp)/hexp_invGyr /3.0)) ! * (1 + <I>/3)

                if(early_step_count .eq. 1)then
                   ! Lagrangian subdomain mass-related constant
                   Aconst(ip,jp,kp) = &
                        1.5_dp  &
                        * hexp_invGyr**2 & !
                        * omega_m &
                        * aexp**3 &
                        ! under-/over-density factor (1 - <I>):
                        * (1.0 - &
                        (divergence_av(ip,jp,kp)/hexp_invGyr) )
                endif
             enddo
          enddo
       enddo

       ! update number of steps available
       n_steps_available = n_steps_available+1
    else
       ! Shift old stored data to one step earlier, but overwriting (losing)
       ! the information at the earliest time step.
       do i_steps = 1, n_steps_max-1
          t_FLRW(i_steps) = t_FLRW(i_steps+1)
          a_FLRW(i_steps) = a_FLRW(i_steps+1)
          H_FLRW(i_steps) = H_FLRW(i_steps+1)

          a_D(i_steps) = a_D(i_steps+1)

          do ip = 1,scal_av_n_gridsize1
             do jp = 1,scal_av_n_gridsize2
                do kp = 1,scal_av_n_gridsize3
                   divergence_history(i_steps, ip,jp,kp) = &
                        divergence_history(i_steps+1, ip,jp,kp)
                   IIinv_history(i_steps, ip,jp,kp) = &
                        divergence_history(i_steps+1, ip,jp,kp)
                   Q_history(i_steps, ip,jp,kp) = &
                        Q_history(i_steps+1, ip,jp,kp)
                   density_history(i_steps, ip,jp,kp) = &
                        density_history(i_steps+1, ip,jp,kp)
                   a_D_local_history(i_steps, ip,jp,kp) = &
                        a_D_local_history(i_steps+1, ip,jp,kp)
                   dot_a_D_local_history(i_steps, ip,jp,kp) = &
                        dot_a_D_local_history(i_steps+1, ip,jp,kp)
                enddo
             enddo
          enddo
       enddo
       ! Store present data: cosmological time
       ! UNITS: all in Gyr, Gyr^{-1}, or dimensionless, as appropriate
       t_FLRW(n_steps_max) = texp *scale_t_Gyr ! global time in Julian Gyr
       a_FLRW(n_steps_max) = aexp
       H_FLRW(n_steps_max) = hexp_invGyr ! FLRW expansion rate in inverse Julian Gyr

       write(*,'(a,g15.6)')"t_FLRW(n_steps_max) = ", &
            t_FLRW(n_steps_max)

       ! store Q
       do ip = 1,scal_av_n_gridsize1
          do jp = 1,scal_av_n_gridsize2
             do kp = 1,scal_av_n_gridsize3
                divergence_history(n_steps_available, ip,jp,kp) = &
                     divergence_av(ip,jp,kp)

                IIinv_history(n_steps_available, ip,jp,kp) = &
                     IIinv_av(ip,jp,kp)

                if(scal_av_set_Q_to_zero)then
                   Q_history(n_steps_available, ip,jp,kp) = 0_dp
                elseif(scal_av_Q_include_vort)then
                   Q_history(n_steps_available, ip,jp,kp) = &
                        2.0_dp * IIinv_av(ip,jp,kp) &
                        - (2.0_dp/3.0_dp) * &
                        divergence_av(ip,jp,kp)**2
                else
                   Q_history(n_steps_available, ip,jp,kp) = &
                        2.0_dp * ( & !vorticity_av(ip,jp,kp) &
                        -shear_av(ip,jp,kp) ) + &
                        (2.0_dp/3.0_dp) * &
                        ( divergence_sq_av(ip,jp,kp) - &
                        divergence_av(ip,jp,kp)**2 )
                endif

                if(scal_av_use_omega_m_FLRW)then
                   density_history(n_steps_available, ip,jp,kp) = &
                        omega_m ! this means \Omega_{m0}^{FLRW}
                else
                   density_history(n_steps_available, ip,jp,kp) = &
                        dtfe_density_av(ip,jp,kp)
                endif

                ! a_D_local_history  calculated below using Raychaudhuri Eq
             enddo
          enddo
       enddo
    endif shift_and_store

    if(n_steps_available .gt. n_steps_max)then
       write(6,'(a,i7,a,i7)')'ERROR: n_steps_available = ',n_steps_available, &
            '> nsteps_max = ',n_steps_max
       stop
    endif


    ! Calculate a_D_local and dot_a_D_local maps if at least 3 steps
    ! are available.
    if(n_steps_available .ge. n_steps_max)then
       a_D(n_steps_available) = 0_dp
       Q_mean = 0_dp
       Q_stddev = 0_dp

       delta_Q_mean = 0_dp
       delta_Q_stddev = 0_dp

       delta_t_FLRW = (t_FLRW(n_steps_available) - t_FLRW(n_steps_available-1))

       main_scalav_loop_ip: do ip = 1,scal_av_n_gridsize1
          main_scalav_loop_jp: do jp = 1,scal_av_n_gridsize2
             main_scalav_loop_kp: do kp = 1,scal_av_n_gridsize3

                ! For analysis purposes; not used directly.
                Q_mean = Q_mean + Q_history( n_steps_available, ip,jp,kp)
                Q_stddev = Q_stddev + Q_history( n_steps_available, ip,jp,kp)**2

                delta_Q = Q_history( n_steps_available, ip,jp,kp) &
                     - Q_history( n_steps_available-1, ip,jp,kp)
                delta_Q_mean = delta_Q_mean + delta_Q
                delta_Q_stddev = delta_Q_stddev + delta_Q*delta_Q

                call get_next_a_D_local_rk4(&
                     !INPUTS
                     delta_t_FLRW, &
                     Aconst(ip,jp,kp), &
                     a_D_local_history(n_steps_available-1, ip,jp,kp), &
                     dot_a_D_local_history(n_steps_available-1, ip,jp,kp), &
                     !Q_history(n_steps_available-2, ip,jp,kp), &
                     Q_history(n_steps_available-1, ip,jp,kp), &
                     Q_history(n_steps_available, ip,jp,kp), &
                     !density_n_minus_1, &
                     !density_n, &
                     ! OUTPUTS
                     a_D_local_history(n_steps_available, ip,jp,kp), &
                     dot_a_D_local_history(n_steps_available, ip,jp,kp) &
                     )

                ! prevent contraction by more than standard virialisation overdensity
                if(a_D_local_history(n_steps_available, ip,jp,kp) .lt. &
                     TOL_A_D_VIRIAL_RATIO * a_D(n_steps_available-1))then
                   a_D_local_history(n_steps_available, ip,jp,kp) = &
                        TOL_A_D_VIRIAL_RATIO * a_D(n_steps_available-1)

                endif
                if(dot_a_D_local_history(n_steps_available, ip,jp,kp) &
                     * delta_t_FLRW &
                     .lt. DOT_A_D_DELTA_T_VIRIAL_MIN)then
                   dot_a_D_local_history(n_steps_available, ip,jp,kp) = &
                        DOT_A_D_DELTA_T_VIRIAL_MIN / &
                        delta_t_FLRW
                endif


                if(scal_av_use_a_FLRW)then ! override
                   a_D_local_history(n_steps_available, ip,jp,kp) = aexp
                endif
                if(scal_av_use_H_FLRW)then ! override
                   dot_a_D_local_history(n_steps_available, ip,jp,kp) = &
                        hexp_invGyr * aexp
                endif

                a_D(n_steps_available) = &
                     a_D(n_steps_available) + &
                     a_D_local_history(n_steps_available, ip,jp,kp)**3

             enddo main_scalav_loop_kp
          enddo main_scalav_loop_jp
       enddo main_scalav_loop_ip


       ! analysis
       Q_mean = Q_mean/ n_subdomains
       if(n_subdomains > 1.0_dp) &
            Q_stddev = sqrt( (Q_stddev / n_subdomains - Q_mean**2) &
            * (n_subdomains/(n_subdomains - 1.0_dp)) )
       write(*,'(a,2es16.5)')'Q::mean,stddev = ', &
            Q_mean,Q_stddev

       delta_Q_mean = delta_Q_mean/ n_subdomains
       if(n_subdomains > 1.0_dp) &
            delta_Q_stddev = sqrt( (delta_Q_stddev / n_subdomains - delta_Q_mean**2) &
            * (n_subdomains/(n_subdomains - 1.0_dp)) )
       write(*,'(a,2es16.5)')'delta_Q:mean,stddev = ', &
            delta_Q_mean,delta_Q_stddev

       ! Global scale factor: assuming initial equipartition in DTFE
       ! grid, a_D is the cube root of the mean volume factor (scale
       ! factor cubed).
       a_D(n_steps_available) = &
            exp((1.0_dp/3.0_dp)*log(a_D(n_steps_available)/ n_subdomains ))
       ! Global expansion rate: estimate from old and new a_D estimates.
       H_D(n_steps_available) = &
            (a_D(n_steps_available) - a_D(n_steps_available-1)) / &
            delta_t_FLRW / &
            (0.5*(a_D(n_steps_available) + a_D(n_steps_available-1)))

       a_D_common = a_D(n_steps_available)
       H_D_common = H_D(n_steps_available) ! in inverse Gyr
       have_a_D_estimate = .true.
       have_H_D_estimate = .true.

       if(scal_av_biscale)then
          write(6,'(a,6es16.5)')'a_plus,a_minus,a_global,aexp,f_antiflow,t_EdS= ', &
               a_D_local_history(n_steps_available, 1,1,1), &
               a_D_local_history(n_steps_available, 2,1,1), &
               a_D_common, &
               aexp, &
               f_antiflow, &
               t_FLRW(n_steps_available) + t_zero_Gyr_EdS ! FLRW foliation time
          write(6,'(a,3es16.5)')'Eul_vol_tot,t_EdS= ', &
               Eul_vol_tot(1,1,1), &
               Eul_vol_tot(2,1,1), &
               t_FLRW(n_steps_available) + t_zero_Gyr_EdS ! FLRW foliation time
          write(6,'(a,7es16.5)')'Q_plus,Q_minus,vort_p,vort_m,H_plus,H_minus,t_EdS= ', &
               Q_history(n_steps_available, 1,1,1), &
               Q_history(n_steps_available, 2,1,1), &
               vorticity_av(1,1,1), &
               vorticity_av(2,1,1), &
               dot_a_D_local_history(n_steps_available, 1,1,1)/ a_D_local_history(n_steps_available, 1,1,1), &
               dot_a_D_local_history(n_steps_available, 2,1,1)/ a_D_local_history(n_steps_available, 2,1,1), &
               t_FLRW(n_steps_available) + t_zero_Gyr_EdS ! FLRW foliation time
       endif
       write(*,'(a,4es16.5)')'aexp,a_D,t_EdS = ',aexp,a_D_common, &
            t_FLRW(n_steps_available) + t_zero_Gyr_EdS,f_antiflow ! FLRW foliation time
       write(*,'(a,2es16.5)')'hexp_kmsMpc,H_D_kmsMpc = ', &
            hexp_kmsMpc, &
            H_D_common * kms_per_MpcGyr

       if(scal_av_no_feedback)then
          a_D_common = aexp ! Newtonian: ignore the calculated a_D_common
       endif

       write(*,'(a,2es16.5)')'Omega_Q_FLRW::mean,stddev = ', &
            -Q_mean/(6.0_dp*hexp_invGyr**2),Q_stddev/(6.0_dp*hexp_invGyr**2)

       if(a_D_common < TOL_A_D_VIRIAL_RATIO * aexp)then
          write(*,'(a,3es16.5)') &
               "Full collapse: a_D_common < TOL_A_D_VIRIAL_RATIO * aexp = ", &
               a_D_common, TOL_A_D_VIRIAL_RATIO, aexp
          !call clean_stop
          stop ! clean_stop doesn't seem to kill mpirun jobs
       endif
    endif


    if(n_steps_available > 1)then
       write(*,'(a,es16.5,a,4es16.5)')'FLRW: da/dt/a\approx', &
            (a_FLRW(n_steps_available) - a_FLRW(n_steps_available-1)) / &
            delta_t_FLRW / &
            (0.5*(a_FLRW(n_steps_available) + a_FLRW(n_steps_available-1))),  &
            ' aexp,hexp_invGyr,_kmsMpc,*a^(1.5)= ',aexp,hexp_invGyr,hexp_kmsMpc, &
            hexp_kmsMpc * aexp*sqrt(aexp)
       ! DEBUG:
       write(*,'(a,5es16.5)')'a_D_local_...= ', &
            a_D_local_history(n_steps_available-1, 1, 1, 1), &
            hexp_invGyr, &
            (t_FLRW(n_steps_available) - t_FLRW(n_steps_available-1)), &
            hexp_invGyr * &
            (t_FLRW(n_steps_available) - t_FLRW(n_steps_available-1)), &
            a_D_local_history(n_steps_available, 1, 1, 1)
    endif


    ! numerical check on simulation parameters
    write(*,'(a,6es16.5)')'sim-params: ', &
         t_FLRW(min(n_steps_available,n_steps_max)), &
         exp(1.5*log(aexp)), &
         t, scale_t, aexp, texp

    if(last_step)then
       ! minimise memory holes by deallocating in reverse order
       deallocate(Aconst)
       deallocate(dot_a_D_local_history)
       deallocate(a_D_local_history)
       deallocate(density_history)
       deallocate(IIinv_history)
       deallocate(divergence_history)
       deallocate(Q_history)
    endif

    return
  end subroutine update_av_scalars_global


  subroutine get_a_D_derivs(&
       !INPUTS - mass-related constant and Q history
       Aconst_copy, & ! TODO: add eq number + ref of Roukema et al 2017
       !Q_n_minus_2, &
       Q_n_minus_1, &
       Q_n, &
       delta_t_Q, &
       ! INPUTS - dummy names for this function only - "_in"
       ! suffix = "input"
       delta_t_since_n_minus_1, &
       a_D_in, &
       a_D_dot_in, &
       ! OUTPUTS
       a_D_in_deriv, &
       a_D_dot_in_deriv)

    implicit none

    !INPUTS - mass-related constant and Q history
    real(dp), intent(in) :: Aconst_copy
    real(dp), intent(in) :: Q_n_minus_1, Q_n, delta_t_Q ! ,Q_n_minus_2
    ! INPUTS - dummy names for this function only
    real(dp), intent(in) :: delta_t_since_n_minus_1
    real(dp), intent(in) :: a_D_in, a_D_dot_in
    ! OUTPUTS
    real(dp), intent(out) :: a_D_in_deriv, a_D_dot_in_deriv

    !INTERNAL
    real(dp) :: frac, Q_interp

    ! derivative of a_D - already known = standard trick for 2nd order ODE
    a_D_in_deriv = a_D_dot_in

    ! derivative of a_D_dot
    ! linear interpolation for value of Q between (n-1)-th and n-th steps
    frac = delta_t_since_n_minus_1 / delta_t_Q
    Q_interp = (1.0_dp - frac)* Q_n_minus_1 + frac * Q_n

    a_D_dot_in_deriv = (Q_interp * a_D_in - Aconst_copy / &
         (min(max(a_D_in, TOL_RAY_FIRST_ORDER_MIN), &
         TOL_RAY_FIRST_ORDER_MAX))**2 ) &
         /3.0_dp

  end subroutine get_a_D_derivs



  ! Given parameters at n-2-th, n-1-th and (some at) the n-th time
  ! step, integrate the Raychaudhuri equation to output
  ! a_D_n and dot_a_D_n.
  subroutine get_next_a_D_local_rk4(&
       !INPUTS
       ! t_FLRW_n_minus_1, & in practice, superfluous
       delta_t_FLRW_in, &
       Aconst_in, &
       a_D_n_minus_1, &
       a_D_dot_n_minus_1, &
       !Q_n_minus_2, &
       Q_n_minus_1, &
       Q_n, &
       !density_n_minus_1, &
       !density_n, &
       ! OUTPUTS
       a_D_n, &
       dot_a_D_n)

    implicit none

    !INPUTS
    !real(dp), intent(in) :: t_FLRW_n_minus_1
    real(dp), intent(in) :: delta_t_FLRW_in
    real(dp), intent(in) :: Aconst_in ! mass-related constant per subdomain
    real(dp), intent(in) :: a_D_n_minus_1
    real(dp), intent(in) :: a_D_dot_n_minus_1
    !real(dp), intent(in) :: Q_n_minus_2
    real(dp), intent(in) :: Q_n_minus_1
    real(dp), intent(in) :: Q_n
    !real(dp), intent(in) :: density_n_minus_1 ! presently not used
    !real(dp), intent(in) :: density_n ! presently not used
    ! OUTPUTS
    real(dp), intent(out) :: a_D_n
    real(dp), intent(out) :: dot_a_D_n

    !INTERNAL
    ! classical Runge-Kutta slopes;
    ! param 1 = a_D
    ! param 2 = a_D_dot
    real(dp), dimension(2) :: rk_k1, rk_k2, rk_k3, rk_k4

    real(dp) :: delta_t_tmp, a_D_tmp, a_D_dot_tmp


    ! RK k1
    delta_t_tmp = 0.0_dp
    call get_a_D_derivs(&
       !INPUTS - fixed
         Aconst_in, &
         !Q_n_minus_2,
         Q_n_minus_1, Q_n, delta_t_FLRW_in, &
         !INPUTS - varying
         delta_t_tmp, a_D_n_minus_1, a_D_dot_n_minus_1, &
         ! OUTPUTS
         rk_k1(1), rk_k1(2))

    ! RK k2
    delta_t_tmp = 0.5_dp * delta_t_FLRW_in
    a_D_tmp = a_D_n_minus_1 + delta_t_tmp * rk_k1(1)
    a_D_dot_tmp = a_D_dot_n_minus_1 + delta_t_tmp * rk_k1(2)
    call get_a_D_derivs(&
         !INPUTS - fixed
         Aconst_in, & !Q_n_minus_2,
         Q_n_minus_1, Q_n, delta_t_FLRW_in, &
         !INPUTS - varying
         delta_t_tmp, a_D_tmp, a_D_dot_tmp, &
         ! OUTPUTS
         rk_k2(1), rk_k2(2))

    ! RK k3
    a_D_tmp = a_D_n_minus_1 + delta_t_tmp * rk_k2(1)
    a_D_dot_tmp = a_D_dot_n_minus_1 + delta_t_tmp * rk_k2(2)
    call get_a_D_derivs(&
       !INPUTS - fixed
         Aconst_in, & !Q_n_minus_2,
         Q_n_minus_1, Q_n, delta_t_FLRW_in, &
         !INPUTS - varying
         delta_t_tmp, a_D_tmp, a_D_dot_tmp, &
         ! OUTPUTS
         rk_k3(1), rk_k3(2))

    ! RK k4
    delta_t_tmp = delta_t_FLRW_in
    a_D_tmp = a_D_n_minus_1 + delta_t_tmp * rk_k3(1)
    a_D_dot_tmp = a_D_dot_n_minus_1 + delta_t_tmp * rk_k3(2)
    call get_a_D_derivs(&
       !INPUTS - fixed
         Aconst_in, & !Q_n_minus_2,
         Q_n_minus_1, Q_n, delta_t_FLRW_in, &
         !INPUTS - varying
         delta_t_tmp, a_D_tmp, a_D_dot_tmp, &
         ! OUTPUTS
         rk_k4(1), rk_k4(2))

    a_D_n = a_D_n_minus_1 + delta_t_FLRW_in * &
         (rk_k1(1) + 2.0_dp * rk_k2(1) + 2.0_dp * rk_k3(1) + rk_k4(1)) &
         /6.0_dp
    dot_a_D_n = a_D_dot_n_minus_1 + delta_t_FLRW_in * &
         (rk_k1(2) + 2.0_dp * rk_k2(2) + 2.0_dp * rk_k3(2) + rk_k4(2)) &
         /6.0_dp
  end subroutine get_next_a_D_local_rk4

end module update_av_scalars_global_module
