#!/bin/bash

# mpgrafic_params: configure, compile, and run mpgrafic
# (C) 2016-2017 B. Roukema GPLv2 or later

# Run this in a directory in which you are happy to produce many output files.

if [ $# == 0 ] ; then
    echo "Usage: mpgrafics_params.sh BOXSIZE_H0EFF_MPC MPGRAFIC_NP [[[LCDM|EdS] N_CORES] SEED]"
    echo "BOXSIZE_H0EFF_MPC = box size in units of Mpc/h_0^{eff}"
    echo "MPGRAFIC_NP = cube root of total number of particles"
    echo "LCDM = LCDM | EdS = choice of flat FLRW model [default EdS]"
    echo "N_CORES = number of cores to use for MPI"
    echo "SEED = seed for pseudo-random number generator"
    exit 0
fi

export N_CORES_DEFAULT=1 # mpgrafic is fast.

# Avoid decimal comma from French or Polish locales.
export LANG=C

export BOXSIZE_H0EFF_MPC=$1
export MPGRAFIC_NP=$2
if [ "x$4" != "x" ]; then
    export N_CORES=$4
    if [ "x$5" != "x" ]; then
        export SEED=$5
    else
        export SEED=$(od -d -An -N2 /dev/random)
    fi
else
    export N_CORES=${N_CORES_DEFAULT}
    export SEED=$(od -d -An -N2 /dev/random)
fi

## H_0^{eff}, H_1^{bg}, cf arXiv:1608.06004
export H0EFF=67.74 # measured at low redshift

if [ "x$3" != "xLCDM" ]; then
    # non-LCDM option = EdS
    export H1BG=37.7 # EdS model fit to high-redshift observations

    export BOXSIZE_H1BG_MPC=$(echo ${BOXSIZE_H0EFF_MPC} ${H0EFF} ${H1BG} | \
                                     awk '{print $1*$3/$2}')
    export FLRW_PARAMS="1 0"
    export SIGMA_8="1.0395"
else
    # LCDM option
    export H1BG=${H0EFF} # H_1^bg \equiv H_0^eff

    export BOXSIZE_H1BG_MPC=${BOXSIZE_H0EFF_MPC}
    export FLRW_PARAMS="0.3089 0.6911"
    export SIGMA_8="0.8159"
fi


echo "Will use mpgrafic option --np=" ${MPGRAFIC_NP}
echo "BOXSIZE_H0EFF_MPC = ${BOXSIZE_H0EFF_MPC}"
echo "BOXSIZE_H1BG_MPC = ${BOXSIZE_H1BG_MPC}"
echo "N_CORES = ${N_CORES}"
echo "SEED = ${SEED}"
echo ""

## run
#time mpirun -np ${N_CORES} ../src/mpgrafic <<EOF # mpgrafic-0.2
## debian system installed:
# time mpirun -np ${N_CORES} mpgrafic --np=${MPGRAFIC_NP} <<EOF
time mpirun -np ${N_CORES} mpgrafic --np=${MPGRAFIC_NP} <<EOF # mpgrafic-0.3.4
4
${FLRW_PARAMS} ${H1BG}
0
1
-${SIGMA_8}
0.001,100
-${BOXSIZE_H1BG_MPC}
1
0




1
${SEED}
whitenoise
0
padding
EOF
