! Copyright (C) 2010-2016 Romain Teyssier et al under the Cecill licence
! See http://www.cecill.info for licence details.
! Co-holders of copyright for this file include Andreas Bleuler,
! Boud Roukema.

module poisson_commons
  use amr_commons
  use poisson_parameters

  real(dp),allocatable,dimension(:)  ::phi,phi_old       ! Potential
  real(dp),allocatable,dimension(:)  ::rho               ! Density
  real(dp),allocatable,dimension(:,:)::f                 ! 3-force

  real(dp),allocatable,dimension(:)  ::rho_top   ! Density at last CIC level

  ! Multigrid lookup table for amr -> mg index mapping
  integer, allocatable, dimension(:) :: lookup_mg   ! Lookup table

  ! Communicator arrays for multigrid levels
  type(communicator), allocatable, dimension(:,:) :: active_mg
  type(communicator), allocatable, dimension(:,:) :: emission_mg

  ! Minimum MG level
  integer :: levelmin_mg

  ! Multigrid safety switch
  logical, allocatable, dimension(:) :: safe_mode

  ! Multipole coefficients
  real(dp),dimension(1:ndim+1)::multipole

contains
  function fourpi_gravity()
    real(dp) :: fourpi_gravity

    if(.not.cosmo)then
       fourpi_gravity = 4.0D0*ACOS(-1.0D0)
    else
       if(.not.scalav .or. .not.have_a_D_estimate)then
          fourpi_gravity = 1.5D0*omega_m*aexp
       else ! if using scalar averaging, the use scalav expansion rate
          fourpi_gravity = 1.5D0*omega_m*a_D_common
       endif
    endif
  endfunction fourpi_gravity

end module poisson_commons
